﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using TestForms.Core.Views.User;
using TestForms.Core.Views.RegisterData;
using System.Threading.Tasks;
using TestForms.Core.Services;
using TestForms.Core.ViewModels.Base;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TestForms
{
    
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            InitApp();

        }

        private void InitApp()
        {
            ViewModelLocator.RegisterDependencies(false);
        }

        private Task InitNavigation()
        {
            var navigationService = ViewModelLocator.Resolve<INavigationService>();
            return navigationService.InitializeAsync();
        }

        protected override async void OnStart()
        {
            base.OnStart();
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                AppCenter.Start("0bc2b0fb-8373-41af-8889-c934530b02d8", typeof(Analytics), typeof(Crashes));
            });
           
            await InitNavigation();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
