﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestForms.Core.ViewModels.Base;

namespace TestForms.Core.ViewModels.Home
{
    public class MainMenuViewModel :ViewModelBase
    {
        public MainMenuViewModel() { }

        public override async Task InitializeAsync(object navigationData)
        {
            await base.InitializeAsync(navigationData);
        }
    }
}
