﻿using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.ViewModels.Base;
using TestForms.Models.Login;
using TestForms.Models.Menu;
using TestForms.Core.ViewModels.User;
using TestForms.Core.ViewModels.RegisterData;
using TestForms.Core.Helpers;

namespace TestForms.Core.ViewModels.Home
{
    public class WarehouseMenuViewModel : ViewModelBase
    {
        public WarehouseMenuViewModel()
        {
            MenuItemsTotal = new ObservableCollection<MenuItem>(new[]
            {
				new MenuItem { Id = 1, Line = false, Icon="homepage", TargetType = typeof(ProgresoCaptacionViewModel) },
                new MenuItem { Id = 2, Line = false, Icon="credit_card_02", TargetType = typeof(DatosPersonalesPrimViewModel) },
                new MenuItem { Id = 3, Line = false, Icon="credit_card", TargetType = typeof(ResumenMesViewModel) },
                new MenuItem { Id = 4, Line = false, Icon="ic_search"}
            });

            MenuItems = new ObservableCollection<MenuItem>();

            Options = new List<OptionModel>();
            //Options = Settings.OptionMenu;
            if(Options != null)
            {
                foreach (var itemTotal in MenuItemsTotal)
                {
                    foreach (var item in Options)
                    {
                        if (itemTotal.Id == item.code && item.is_Visible == 1)
                        {
                            MenuItems.Add(new MenuItem() { Id = itemTotal.Id, Line = false, Icon = itemTotal.Icon, TargetType = itemTotal.TargetType, Title = item.name });
                        }
                    }
                }
            }
        }
        public async Task<bool> OnMenuItemSelected_NonView(MenuItem item)
        {
            if (IsBusy)
            {
                return true;
            }
            IsBusy = true;
            try
            {
                switch (item.Id)
                {
                    case 7:
                        IsBusy = true;

                        // Logout
                       // await NavigationService.NavigateToAsync<LoginViewModel>(true);
                        //await NavigationService.RemoveBackStackAsync();
                        IsBusy = false;
                        break;
                    default: // something 
                        break;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;
            }
            return true;
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        private string _versionNumber;
        public string VersionNumber
        {
            get { return _versionNumber; }
            set
            {
                _versionNumber = value;
                RaisePropertyChanged(() => VersionNumber);
            }
        }


        public async override Task InitializeAsync(object navigationData)
        {
            //UserName = Settings.UserName;
            var versionActual = CrossDeviceInfo.Current;
            VersionNumber = versionActual.AppVersion;
        }
        
        public ObservableCollection<MenuItem> MenuItems { get; set; }
        public ObservableCollection<MenuItem> MenuItemsTotal { get; set; }
        public List<OptionModel> Options { get; set; }


        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged == null)
                return;

            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        public ICommand CloseSession => new Xamarin.Forms.Command(async () => await GotoLogin());
        private async Task GotoLogin()
        {
            IsBusy = true;
           

            Settings.GeneralSettings = String.Empty;
            await NavigationService.NavigateToAsync<MainPageViewModel>();
            await NavigationService.RemoveBackStackAsync();
            IsBusy = false;
        }

    }
}
