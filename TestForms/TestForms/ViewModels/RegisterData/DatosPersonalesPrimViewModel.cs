﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.ViewModels.User;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.RegisterData
{
    public class DatosPersonalesPrimViewModel:ViewModelBase
    {
        string _entryDNI;
        public string DniNumber
        {
            get { return _entryDNI; }
            set
            {
                _entryDNI = value;

                IsTextDniChanging = (_entryDNI == string.Empty ? false : true);
                IsCheckDniVisible = (_entryDNI == string.Empty ? false : true);
                RaisePropertyChanged(() => DniNumber);
            }
        }

        private bool _isTextdniChanging;
        public bool IsTextDniChanging
        {
            get
            {
                return _isTextdniChanging;
            }
            set
            {
                _isTextdniChanging = value;
                RaisePropertyChanged(() => IsTextDniChanging);
            }
        }
        
        private bool _isFocusedEntryDNI;
        public bool IsFocusedEntryDNI
        {
            get
            {
                return _isFocusedEntryDNI;
            }
            set
            {
                _isFocusedEntryDNI = value;
                IsCheckDniVisible = (_isFocusedEntryDNI ? false : true);
                RaisePropertyChanged(() => IsFocusedEntryDNI);
            }
        }

        private bool _isCheckDniVisible;
        public bool IsCheckDniVisible
        {
            get
            {
                return _isCheckDniVisible;
            }
            set
            {
                _isCheckDniVisible = value;
                RaisePropertyChanged(() => IsCheckDniVisible);
            }
        }

        private string _entryCelular;
        public string EntryCelular
        {
            get { return _entryCelular; }
            set
            {
                _entryCelular = value;


                IsCheckCelVisible = (_entryCelular == string.Empty ? false : true);
                RaisePropertyChanged(() => EntryCelular);
            }
        }

      
      
        private bool _isCheckCelVisible;
        public bool IsCheckCelVisible
        {
            get
            {
                return _isCheckCelVisible;
            }
            set
            {
                _isCheckCelVisible = value;
                RaisePropertyChanged(() => IsCheckCelVisible);
            }
        }

        private string _entryEMail;
        public string EntryEMail
        {
            get { return _entryEMail; }
            set
            {
                _entryEMail = value;


                IsVisibleCkeckEMail = (_entryEMail == string.Empty ? false : true);
                RaisePropertyChanged(() => EntryEMail);
            }
        }



        private bool _isVisibleCkeckEMail;
        public bool IsVisibleCkeckEMail
        {
            get
            {
                return _isVisibleCkeckEMail;
            }
            set
            {
                _isVisibleCkeckEMail = value;
                RaisePropertyChanged(() => IsVisibleCkeckEMail);
            }
        }

        public override async Task InitializeAsync(object navigationData)
        {
            try
            {
                
                IsBusy = false;
               
                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;

            }
        }

        public ICommand goToPersonalInfoDirCommand => new Command(async () => await goToPersonalInfoDir());
        private async Task goToPersonalInfoDir()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<DatosPersonalesDirViewModel>();
                await NavigationService.RemoveBackStackAsync();


            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

        public ICommand CancelSaleCommand => new Command(async () => await CancelSale());
        private async Task CancelSale()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<ProgresoCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();


            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

    }

}
