﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.ViewModels.User;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.RegisterData
{
    public class DatosAcademicosViewModel:ViewModelBase
    {
        public override async Task InitializeAsync(object navigationData)
        {
            try
            {

                IsBusy = false;

                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;

            }
        }

        public ICommand goToConfirmationPageCommand => new Command(async () => await goToConfirmationPage());
        private async Task goToConfirmationPage()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<IngresoConfirmacionViewModel>();
                await NavigationService.RemoveBackStackAsync();


            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

        public ICommand CancelSaleCommand => new Command(async () => await CancelSale());
        private async Task CancelSale()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<ProgresoCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();


            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }
    }

}
