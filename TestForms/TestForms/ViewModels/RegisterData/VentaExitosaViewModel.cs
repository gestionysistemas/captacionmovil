﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.ViewModels.User;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.RegisterData
{
    public class VentaExitosaViewModel:ViewModelBase
    {
        public override async Task InitializeAsync(object navigationData)
        {
            try
            {

                IsBusy = false;

                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;

            }
        }

        public ICommand goToLandingPageCommand => new Command(async () => await goToLandingPage());
        private async Task goToLandingPage()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<ProgresoCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();


            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }
    }

}
