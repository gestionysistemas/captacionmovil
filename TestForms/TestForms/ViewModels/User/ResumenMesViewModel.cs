﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.ViewModels.User;
using Microcharts;
using Microcharts.Forms;
using SkiaSharp;
using TestForms.Core.Services.User;
using TestForms.Models.User;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.User
{
    public class ResumenMesViewModel : ViewModelBase
    {
        private IUserService _userService;
        private Chart _barChart;

        public ResumenMesViewModel(IUserService userService) {
            _userService = userService;
        }

        public Chart BarChart
        {
            get { return _barChart; }
            set {
                _barChart = value;
                RaisePropertyChanged(() => BarChart);
            }
        }
        
        public override async Task InitializeAsync(object navigationData)
        {
          List<UserSalesModel> listUserSalesModel = await _userService.GetListUserSalesByMonth();
          BarChartConstruct(listUserSalesModel);
        }

        void BarChartConstruct(List<UserSalesModel> listUserSalesModel)
        {
          /*  BarChart _bChart = new BarChart();
           // Entry[] entries = new Entry[listUserSalesModel.Count];
            int i = 0;

            foreach (UserSalesModel item in listUserSalesModel)
            {
                entries[i].Color = SKColor.Parse("#ff80ff");
                entries[i].TextColor = SKColor.Parse("#ff80ff");
                entries[i].Label = DateTime.Parse(item.SaleDate).Month.ToString();
                entries[i].ValueLabel = item.AmountSale.ToString();

            }

           // _barChart.Entries = entries;
            _barChart.LabelTextSize = 20;
            BarChart = _bChart;*/
       
        }

        public ICommand GoToSearchInfoCardCommand => new Command(async () => await GoToSearchInfoCard());
        private async Task GoToSearchInfoCard()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<InicioCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

    }

}
