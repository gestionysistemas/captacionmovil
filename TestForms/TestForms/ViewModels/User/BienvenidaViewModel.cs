﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.ViewModels.Base;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.User
{
    public class BienvenidaViewModel:ViewModelBase
    {

        private bool isTextChanging;
        public bool IsTextChanging
        {
            get
            {
                return isTextChanging;
            }
            set
            {
                isTextChanging = value;
                RaisePropertyChanged(() => IsTextChanging);
            }
        }

        private string dni;
        public string Dni
        {
            get
            {
                return dni;
            }
            set
            {
                dni = value;
                IsTextChanging = (dni == string.Empty ? false : true);
                RaisePropertyChanged(() => Dni);
            }
        }

        public override async Task InitializeAsync(object navigationData)
        {
            try
            {
                IsBusy = false;
                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;

            }
        }

        public ICommand GoToLoginPageCommand => new Command(async () => await GoToLoginPage());
        private async Task GoToLoginPage()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<MainPageViewModel>();
                await NavigationService.RemoveBackStackAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

        public ICommand GoToLandingPageCommand => new Command(async () => await GoToLandingPage());
        private async Task GoToLandingPage()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<ProgresoCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }
    }

}
