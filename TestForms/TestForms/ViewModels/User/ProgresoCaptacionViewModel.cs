﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.ViewModels.Base;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.User
{
    public class ProgresoCaptacionViewModel:ViewModelBase
    {
        public override async Task InitializeAsync(object navigationData)
        {
            try
            {
                IsBusy = false;
                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;

            }
        }

        public ICommand GoToSearchInfoCardCommand => new Command(async () => await GoToSearchInfoCard());
        private async Task GoToSearchInfoCard()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<InicioCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }
    }

}
