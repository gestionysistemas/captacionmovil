﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestForms.Core.Helpers;
using TestForms.Core.Services;
using TestForms.Core.Services.User;
using TestForms.Core.ViewModels.Base;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.User
{
    public class MainPageViewModel:ViewModelBase
    {
        private readonly IUserService _userService;
       /*private readonly IParameterService _parameterService;
        private ILoginService _loginService;
        private IIdentityService _identityService;*/
        private string _employeeCode;
        private string _password;

        public MainPageViewModel(IUserService userService)
        {
            _userService = userService;
        }

        public string EmployeeCode
        {
            get { return _employeeCode; }
            set { _employeeCode = value;
                IsTextCodeChanging = (_employeeCode == string.Empty ? false : true);
            }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value;
                  IsTextDniChanging = (_password == string.Empty ? false : true);
            }
        }

        
       private bool _isToggledActive;
        public bool IsToggledActive
        {
            get
            {
                return _isToggledActive;
            }
            set
            {
                _isToggledActive = value;
                RaisePropertyChanged(() => IsToggledActive);
            }
        }

        private bool isTextCodeChanging;
        public bool IsTextCodeChanging
        {
            get
            {
                return isTextCodeChanging;
            }
            set
            {
                isTextCodeChanging = value;
                RaisePropertyChanged(() => IsTextCodeChanging);
            }
        }
        private bool isTextDniChanging;
        public bool IsTextDniChanging
        {
            get
            {
                return isTextDniChanging;
            }
            set
            {
                isTextDniChanging = value;
                RaisePropertyChanged(() => IsTextDniChanging);
            }
        }

        private bool _switchEnabled;
        public bool SwitchEnabled
        {
            get
            {
                return _switchEnabled;
            }
            set
            {
                _switchEnabled = value;
                RaisePropertyChanged(() => SwitchEnabled);
            }
        }

        
        public override async Task InitializeAsync(object navigationData)
        {
            try
            {
                IsBusy = false;
                SwitchEnabled = false;
                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;
               
            }
        }

        

        public ICommand SwitchChangeCommand => new Command(async () => await SwitchChange());
        private async Task SwitchChange()
        {
            try
            {
                IsBusy = true;

              
                
                await NavigationService.NavigateToAsync<ProgresoCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }
        public ICommand GoToMainMenuCommand => new Command(async () => await GoToMainMenu());
        private async Task GoToMainMenu()
        {
            try
            {
                IsBusy = true;

              
                Settings.EmployeeCode = (IsToggledActive ? EmployeeCode : String.Empty);

                await NavigationService.NavigateToAsync<ProgresoCaptacionViewModel>();
                await NavigationService.RemoveBackStackAsync();
                           
            }
            catch (Exception ex)
            {
                IsBusy = false;
               // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

    }

}
