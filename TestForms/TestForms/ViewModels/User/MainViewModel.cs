﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Plugin.DeviceInfo;
using TestForms.Core.ViewModels.Base;

namespace TestForms.Core.ViewModels
{
   public  class MainViewModel: ViewModelBase
   {
        private string getDateTime;
        public string GetDatetime
        {
            get { return getDateTime; }
            set
            {
                getDateTime = value;
                
            }
        }

        private string getVersionSucursal;
        public string GetVersionSucursal
        {
            get { return getVersionSucursal; }
            set
            {
                getVersionSucursal = value;
                
            }
        }

        private DateTime _fechaInicio;
        public DateTime FechaInicio
        {
            get
            {
                _fechaInicio = DateTime.Now;
                return _fechaInicio;
            }
            set
            {
                if (_fechaInicio == value)
                    return;

                _fechaInicio = value;
               
            }
        }

        private DateTime _fechaMin;

        public DateTime FechaMin
        {
            get
            {
                FechaFin = DateTime.Now;
                return FechaFin;
            }
            set
            {
                _fechaMin = value;
               
            }
        }
        private DateTime _fechaMax;

        public DateTime FechaMax
        {
            get { return _fechaMax; }
            set
            {
                _fechaMax = value;

                
            }
        }

        private DateTime _fechaFin;
        public DateTime FechaFin
        {
            get { return _fechaFin; }
            set
            {

                if (_fechaFin == value)
                    return;
                _fechaFin = value;
               
            }
        }

        public override async Task InitializeAsync(object navigationData)
        {
            try
            {
                IsBusy = false;
              
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;
               
            }
        }

    }
}
