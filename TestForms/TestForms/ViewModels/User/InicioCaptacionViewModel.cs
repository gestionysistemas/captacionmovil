﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.ViewModels.RegisterData;
using TestForms.Core.Constants;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestForms.Core.ViewModels.User
{
    public class InicioCaptacionViewModel:ViewModelBase
    {

        string _userDni;

        string _dniNumber;
        public string DniNumber
        {
            get { return _dniNumber; }
            set
            {
                _dniNumber = value;

                IsTextDniChanging = (_dniNumber == string.Empty ? false : true);
                RaisePropertyChanged(() => DniNumber);
            }
        }

        private bool _isTextdniChanging;
        public bool IsTextDniChanging
        {
            get
            {
                return _isTextdniChanging;
            }
            set
            {
                _isTextdniChanging = value;
                RaisePropertyChanged(() => IsTextDniChanging);
            }
        }

        bool _isInitSaleButtonEnable;

        public bool IsInitSaleButtonEnable
        {
            get { return _isInitSaleButtonEnable; }
            set
            {
                _isInitSaleButtonEnable = value;
                RaisePropertyChanged(() => IsInitSaleButtonEnable);
            }
        }

        Color _buttonEnableDisableColor;

        public Color ButtonEnableDisableColor
        {
            get { return _buttonEnableDisableColor; }
            set
            {
                _buttonEnableDisableColor = value;
                RaisePropertyChanged(() => ButtonEnableDisableColor);
            }
        }

        bool _isHeaderMessageVisible= true;

        public bool IsHeaderMessageVisible
        {
            get { return _isHeaderMessageVisible; }
            set { _isHeaderMessageVisible = value;
                RaisePropertyChanged(() => IsHeaderMessageVisible);
            }
        }

        bool _isFailMessageVisible = false;
        public bool IsFailMessageVisible
        {
            get { return _isFailMessageVisible; }
            set
            {
                _isFailMessageVisible = value;
                RaisePropertyChanged(() => IsFailMessageVisible);
            }
        }
        bool _isSuccessMessageVisible = false;
        public bool IsSuccessMessageVisible
        {
            get { return _isSuccessMessageVisible; }
            set
            {
                _isSuccessMessageVisible = value;
                RaisePropertyChanged(() => IsSuccessMessageVisible);
            }
        }
        public override async Task InitializeAsync(object navigationData)
        {
            try
            {
                _userDni = Constants.Constants.DNI;
                IsBusy = false;
                IsHeaderMessageVisible = true;
                IsSuccessMessageVisible = false;
                IsFailMessageVisible = false;
                IsInitSaleButtonEnable = false;
                ButtonEnableDisableColor = Color.FromHex("#BDBDBD");
                await base.InitializeAsync(navigationData);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy = false;

            }
        }

        public ICommand SearchInfoCardCommand => new Command(async () => await SearchInfoCard());
        private async Task SearchInfoCard()
        {
            try
            {
                IsBusy = true;
                IsHeaderMessageVisible = false;

                if (DniNumber == Constants.Constants.DNI)
                {
                    IsFailMessageVisible = false;
                    IsSuccessMessageVisible = true;
                    IsInitSaleButtonEnable = true;
                    ButtonEnableDisableColor = Color.FromHex("#8C4799");
                }
                else
                {
                    IsFailMessageVisible = true;
                    IsSuccessMessageVisible = false;
                    IsInitSaleButtonEnable = false;
                    ButtonEnableDisableColor = Color.FromHex("#BDBDBD");

                }
                

            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

        public ICommand GoToInitSaleCommand => new Command(async () => await GoToInitSale());
        private async Task GoToInitSale()
        {
            try
            {
                IsBusy = true;

                await NavigationService.NavigateToAsync<DatosPersonalesPrimViewModel>();
                await NavigationService.RemoveBackStackAsync();


            }
            catch (Exception ex)
            {
                IsBusy = false;
                // await DialogService.ShowAlertAsync("Advertencia", Constants.Dialog.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }

        }

    }

}
