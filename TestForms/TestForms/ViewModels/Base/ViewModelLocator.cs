﻿using Autofac;
using System;
using System.Globalization;
using System.Reflection;
using Xamarin.Forms;
using TestForms.Core.ViewModels.User;
using TestForms.Core.ViewModels.Home;
using TestForms.Core.ViewModels.RegisterData;
using TestForms.Core.Services;
using TestForms.Core.Services.User;

namespace TestForms.Core.ViewModels.Base
{
    public static class ViewModelLocator
    {
        #region Public Fields

        public static readonly BindableProperty AutoWireViewModelProperty =
            BindableProperty.CreateAttached("AutoWireViewModel", typeof(bool), typeof(ViewModelLocator), default(bool), propertyChanged: OnAutoWireViewModelChanged);

        #endregion Public Fields

        #region Private Fields

        private static IContainer _container;

        #endregion Private Fields

        #region Public Properties

        public static bool UseMockService { get; set; }

        #endregion Public Properties

        #region Public Methods

        public static bool GetAutoWireViewModel(BindableObject bindable)
        {
            return (bool)bindable.GetValue(ViewModelLocator.AutoWireViewModelProperty);
        }

        public static void RegisterDependencies(bool useMockServices)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MainMenuViewModel>();
            builder.RegisterType<WarehouseMenuViewModel>();
            builder.RegisterType<HomeWarehouseViewModel>();
            builder.RegisterType<MainPageViewModel>();
            builder.RegisterType<BienvenidaViewModel>();
            builder.RegisterType<ProgresoCaptacionViewModel>();
            builder.RegisterType<InicioCaptacionViewModel>();
            builder.RegisterType<ResumenMesViewModel>();
            builder.RegisterType<VentaExitosaViewModel>();
            builder.RegisterType<IngresoConfirmacionViewModel>();
            builder.RegisterType<GenerarCodigoViewModel>();
            builder.RegisterType<EnvioDocumentosViewModel>();
            builder.RegisterType<DatosPersonalesPrimViewModel>();
            builder.RegisterType<DatosPersonalesDirViewModel>();
            builder.RegisterType<DatosAcademicosViewModel>();
            builder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            builder.RegisterType<UserMockService>().As<IUserService>();
            /*builder.RegisterType<ParameterService>().As<IParameterService>();
            builder.RegisterType<DeviceService>().As<IDeviceService>();
            builder.RegisterType<PackageService>().As<IPackageService>();
            builder.RegisterType<ManifestService>().As<IManifestService>();
            builder.RegisterType<LoginService>().As<ILoginService>();*/

            if (useMockServices)
            {
                UseMockService = true;
            }
            else
            {
                UseMockService = false;
            }

            if (_container != null)
            {
                _container.Dispose();
            }
            _container = builder.Build();
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static void SetAutoWireViewModel(BindableObject bindable, bool value)
        {
            bindable.SetValue(ViewModelLocator.AutoWireViewModelProperty, value);
        }

        #endregion Public Methods

        #region Private Methods

        private static void OnAutoWireViewModelChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Element;
            if (view == null)
            {
                return;
            }

            var viewType = view.GetType();
            var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}Model, {1}", viewName, viewAssemblyName);

            var viewModelType = Type.GetType(viewModelName);
            if (viewModelType == null)
            {
                return;
            }

            try
            {
                var viewModel = _container.Resolve(viewModelType);
                view.BindingContext = viewModel;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                throw;
            }
          
        }

        #endregion Private Methods
    }
}