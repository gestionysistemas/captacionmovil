﻿using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using TestForms.Core.Services;

namespace TestForms.Core.ViewModels.Base
{
    public abstract class ViewModelBase : ExtendedBindableObject
    {
        #region Protected Fields

       
        protected readonly INavigationService NavigationService;

        #endregion Protected Fields

        #region Private Fields

        private bool _isBusy;
        private bool _isConnected;
        private bool _isModal = false;
        private bool _isConsulting = false;
        private object _navigationData;

        #endregion Private Fields

        #region Public Constructors

        protected ViewModelBase()
        {
            
            NavigationService = ViewModelLocator.Resolve<INavigationService>();
            IsConnected = true;
            _isConnected = true;
        }

        #endregion Public Constructors

        #region Public Properties

        public bool IsLoaded;

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                _isConnected = value;
                RaisePropertyChanged(() => IsConnected);
            }
        }

        #endregion Public Properties

        #region Public Methods

        public virtual async Task InitializeAsync(object navigationData)
        {
            IsConnected = true;
            _navigationData = navigationData;
            Plugin.Connectivity.CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                await TryConnectAsync();
            };

            IsConnected = true;
           // IsConnected = await ConnectivityHelper.CheckConnectivityAsync();
        }

        #endregion Public Methods

        #region Private Methods

        private async Task TryConnectAsync()
        {
            if (_isConsulting) { return; }

            try
            {
                _isConsulting = true;
                //IsConnected = await ConnectivityHelper.CheckConnectivityAsync();
                if (IsConnected)
                {
                    if (!IsLoaded)
                    {
                        await InitializeAsync(_navigationData);
                    }
                }
            }
            catch (Exception)
            {
                //Do nothing
            }
            finally
            {
                _isConsulting = false;
            }
        }
        public bool IsModal
        {
            get
            {
                return _isModal;
            }

            set
            {
                _isModal = value;
                RaisePropertyChanged(() => IsModal);
            }
        }
        public ICommand CloseModalCommand => new Command(async () => await CloseModal());
        public virtual async Task CloseModal()
        {
            IsModal = false;
        }
        #endregion Private Methods
    }
}