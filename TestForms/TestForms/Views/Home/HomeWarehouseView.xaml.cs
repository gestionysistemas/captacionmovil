﻿using TestForms.Core.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestForms.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeWarehouseView : ContentPage
	{
        public HomeWarehouseView()
        {
            InitializeComponent();
        }
    }
}