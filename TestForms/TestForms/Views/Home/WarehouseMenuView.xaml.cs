﻿using TestForms.Core.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TestForms.Core.ViewModels;

namespace TestForms.Core.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WarehouseMenuView : ContentPage
	{
        public ListView ListView;
        public WarehouseMenuView ()
		{
			InitializeComponent ();
            ListView = MenuItemsListView;
            load();
		}

        public async void load()
        {
            await ((WarehouseMenuViewModel)BindingContext).InitializeAsync(null);  
        }
    }
}