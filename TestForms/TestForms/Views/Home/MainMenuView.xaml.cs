﻿using System;
using System.Reflection;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestForms.Core.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainMenuView : MasterDetailPage
	{
        public MainMenuView()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }
        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as Models.Menu.MenuItem;
            if (item == null)
                return;
            IsPresented = false;
            if (item.TargetType.GetTypeInfo().BaseType == (typeof(ContentPage)))
            {
                var page = (Page)Activator.CreateInstance(item.TargetType);
                var navigationPage = new CustomNavigationView(page);
                page.Title = item.Title;
                MasterPage.ListView.SelectedItem = null;
                Detail = (navigationPage);
                await ((ViewModelBase)page.BindingContext).InitializeAsync(null);
            }
            else
            {
                await ((WarehouseMenuViewModel)MasterPage.BindingContext).OnMenuItemSelected_NonView(item);
                MasterPage.ListView.SelectedItem = null;
                return;
            }


            MasterPage.ListView.SelectedItem = null;
        }
    }
}