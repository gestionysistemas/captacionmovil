﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.DeviceInfo;
using Microsoft.AppCenter.Analytics;
using TestForms.Core.ViewModels;
using System.Globalization;
using Xamarin.Forms.Xaml;
using TestForms.Core.ViewModels.Base;
using TestForms.Core.Helpers;

namespace TestForms.Core.Views.User
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPageView : ContentPage
    {
        public MainPageView()
        {
            InitializeComponent();
            
        }
        private void OnToggledControl(object sender, EventArgs e)
        { 
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker
           
            Settings.EmployeeCode = (SwitchInit.IsToggled ? "1234" : String.Empty);
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}
