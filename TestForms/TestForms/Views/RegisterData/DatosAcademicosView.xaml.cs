﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.DeviceInfo;
using Microsoft.AppCenter.Analytics;
using TestForms.Core.ViewModels;
using TestForms.Core.Views.Home;
using System.Globalization;

namespace TestForms.Core.Views.RegisterData
{
    public partial class DatosAcademicosView : ContentPage
    {
        public DatosAcademicosView()
        {
            InitializeComponent();
            
        }

        void Handle_Activated(object sender, System.EventArgs e)
        {
           App app = Application.Current as App; // Get the current App instance
           var mdPage = app.MainPage as MainMenuView; // MainPage should be the type of your MasterDetailPage subclass
           mdPage.IsPresented = true; // present the master page

        }
    }
}
