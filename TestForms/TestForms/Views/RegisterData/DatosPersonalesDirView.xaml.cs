﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.DeviceInfo;
using Microsoft.AppCenter.Analytics;
using TestForms.Core.ViewModels;
using System.Globalization;

namespace TestForms.Core.Views.RegisterData
{
    public partial class DatosPersonalesDirView : ContentPage
    {
        public DatosPersonalesDirView()
        {
            InitializeComponent();
            
        }
        private void OnPickerSelectedDepIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerDep.IsVisible = true;
        }
        private void OnPickerSelectedProvIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerProv.IsVisible = true;
        }
        private void OnPickerSelectedDistIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerDist.IsVisible = true;
        }
        private void OnPickerSelectedUrbIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerUrb.IsVisible = true;
        }
        private void OnPickerSelectedViaIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerVia.IsVisible = true;
        }
        private void OnPickerSelectedIntIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerInterior.IsVisible = true;
        }
        private void OnPickerSelectedAgrupIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;
            var selectedItem = picker.SelectedItem; // This is the model selected in the picker

            imageCheckPickerEtapa.IsVisible = true;
        }
       
       
       
    }
}
