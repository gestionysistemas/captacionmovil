﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestForms.Models.User
{
    public class UserSalesModel
    {
        string saleDate;
        int amountSale;

        public string SaleDate { get => saleDate; set => saleDate = value; }
        public int AmountSale { get => amountSale; set => amountSale = value; }
    }
}
