﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestForms.Models.Chart
{
    public class BarChart
    {
        List<BarChartModel> barChartEntries;
        int LabelTextSize;
        string LabelOrientation;
    }
    public class BarChartModel
    {
        string color;
        string textColor;
        string label;
        string valueLabel;
        
        public string ValueLabel { get => valueLabel; set => valueLabel = value; }
        public string Label { get => label; set => label = value; }
        public string TextColor { get => textColor; set => textColor = value; }
        public string Color { get => color; set => color = value; }
    }
}
