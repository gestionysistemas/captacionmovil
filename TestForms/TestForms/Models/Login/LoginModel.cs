﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestForms.Models.Login
{
    public class LoginOutputModel
    {
        public int id { get; set; }
        public int code { get; set; }
        public string name { get; set; }
        public int ordering { get; set; }
        public string description { get; set; }
        public List<ModuleModel> modules { get; set; }
    }

    public class ModuleModel
    {
        public int id { get; set; }
        public int code { get; set; }
        public int applicationId { get; set; }
        public string name { get; set; }
        public int ordering { get; set; }
        public string description { get; set; }
        public List<OptionModel> options { get; set; }
    }

    public class OptionModel
    {
        public int id { get; set; }
        public int code { get; set; }
        public int moduleId { get; set; }
        public string name { get; set; }
        public int ordering { get; set; }
        public string description { get; set; }
        public int option_Id { get; set; }
        public int is_Visible { get; set; }
        public List<OptionModel> option { get; set; }
        public List<ActionModel> actions { get; set; }
    }

    public class ActionModel
    {
        public int id { get; set; }
        public int code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int optionId { get; set; }
        public int is_Visible { get; set; }
    }

    public class LoginInputModel
    {
        public int AplicacionId { get; set; }
    }
}
