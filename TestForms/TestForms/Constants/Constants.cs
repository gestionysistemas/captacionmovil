﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestForms.Core.Constants
{
    public class Constants
    {
        public  const string EMPLOYEE_CODE = "1234";
        public  const string DNI = "12345678";
        public  const string DISABLE_BUTTON_COLOR = "#BDBDBD";
        public const string  ENABLE_BUTTON_COLOR = "#8C4799";
    }

}
