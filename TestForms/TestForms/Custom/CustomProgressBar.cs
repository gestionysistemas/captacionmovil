﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TestForms.Core.Custom
{
    public class CustomProgressBar:ProgressBar
    {
        public CustomProgressBar() {
        }

        public static readonly BindableProperty ImageSourceProperty =
        BindableProperty.Create("ImageSourceOf", typeof(ImageSource), typeof(ImageSource));

        public ImageSource ImageSourceOf
        {
            get { return GetValue(ImageSourceProperty) as ImageSource; }
            set { SetValue(ImageSourceProperty, value); }
        }

       /* public static readonly BindableProperty ProgressColorProperty =
           BindableProperty.Create("ProgressColor", typeof(Color), typeof(CustomProgressBar), Color.Red, BindingMode.TwoWay, null, null);

        public Color ProgressColor
        {
            get { return (Color)GetValue(ProgressColorProperty); }
            set { SetValue(ProgressColorProperty, value); }
        }*/
        
        public static readonly BindableProperty BackColorProperty =

            BindableProperty.Create("BackColor", typeof(Color), typeof(CustomProgressBar), Color.Blue, BindingMode.TwoWay, null, null);

        public Color BackColor
        {
            get { return (Color)GetValue(BackColorProperty); }
            set { SetValue(BackColorProperty, value); }
        }

        public event EventHandler ValueChanged;

        public void NotifyValueChanged()
        {
            if (ValueChanged != null)
            {
              ValueChanged(this, new EventArgs());
            }
        }
    }
}
