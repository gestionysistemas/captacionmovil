﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TestForms.Core.Custom
{
    public class CustomPicker:Picker
    {
        public CustomPicker() {
        }

        public static readonly BindableProperty EnterTextProperty = BindableProperty.Create(propertyName: "Placeholder", returnType: typeof(string), declaringType: typeof(CustomPicker), defaultValue: default(string));
        public string Placeholder
        {
            get;
            set;
        }

        public static readonly BindableProperty BorderColorProperty =
      BindableProperty.Create(nameof(BorderColor),
          typeof(Color), typeof(CustomPicker), Color.Gray);
        // Gets or sets BorderColor value  
        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }   
    }
}
