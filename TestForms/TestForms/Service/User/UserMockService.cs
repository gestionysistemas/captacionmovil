﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestForms.Models.User;

namespace TestForms.Core.Services.User
{
    public class UserMockService :IUserService
    {
        List<UserSalesModel> listUser = new List<UserSalesModel>()
        {
            new UserSalesModel(){
                SaleDate = DateTime.Now.ToShortDateString() ,
                AmountSale=5 },
            new UserSalesModel(){
                SaleDate = DateTime.Now.AddMonths(1).ToShortDateString() ,
                AmountSale=20 },
            new UserSalesModel(){
                SaleDate = DateTime.Now.AddMonths(2).ToShortDateString() ,
                AmountSale=10 },
            new UserSalesModel(){
                SaleDate = DateTime.Now.AddMonths(3).ToShortDateString() ,
                AmountSale=15 },

            
        };

        public async Task<List<UserSalesModel>> GetListUserSalesByMonth() {
            return listUser.ToList();
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public void ShowMainPage()
        {
            throw new NotImplementedException();
        }
    }
}
