﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestForms.Models.User;

namespace TestForms.Core.Services.User
{
    public interface IUserService
    {
        Task<List<UserSalesModel>> GetListUserSalesByMonth();
        void ShowMainPage();
        void Logout();
    }
}
