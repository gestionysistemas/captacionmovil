﻿using TestForms.Core.ViewModels.Base;
using System.Threading.Tasks;

namespace TestForms.Core.Services
{
    public interface INavigationService
    {
        ViewModelBase PreviousPageViewModel { get; }

        Task InitializeAsync();

        Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase;

        Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase;

        Task RemoveLastFromBackStackAsync();

        Task RemoveBackStackAsync();

        Task PopToRoot(bool Inactive = false);

        Task NavigateBackAsync();
    }
}