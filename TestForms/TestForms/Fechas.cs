﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestForms
{
    public class Fechas
    {
        DateTime _fechaInicio;
        DateTime _fechaFin;
        DateTime _fechaMin;
        DateTime _fechaMax;


      
        public DateTime FechaInicio
        {
            get
            {
                
                return _fechaInicio;
            }
            set
            {
                if (_fechaInicio == value)
                    return;

                _fechaInicio = value;

            }
        }

       

        public DateTime FechaMin
        {
            get
            {
               
                return FechaFin;
            }
            set
            {
                _fechaMin = value;

            }
        }
       
        public DateTime FechaMax
        {
            get { return _fechaMax; }
            set
            {
                _fechaMax = value;


            }
        }

      
        public DateTime FechaFin
        {
            get { return _fechaFin; }
            set
            {

                if (_fechaFin == value)
                    return;
                _fechaFin = value;

            }
        }
    }
}
