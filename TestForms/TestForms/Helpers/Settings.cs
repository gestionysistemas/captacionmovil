using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Collections.Generic;
using TestForms.Core.ViewModels.User;

namespace TestForms.Core.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters.
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;
        private const string EmployeeCodeKey = "EmployeeCodeKey";
       
        #endregion Setting Constants

        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }
        public static string EmployeeCode
        {
            get
            {
                return AppSettings.GetValueOrDefault(EmployeeCodeKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(EmployeeCodeKey, value);
            }
        }

       

    

        public static object DeviceId { get; internal set; }

    }
}