﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using TestForms.Droid.Render;
using TestForms.Core.Custom;
using Android.Graphics.Drawables;
using Android.Graphics;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(MyProgressBarRenderer))]
namespace TestForms.Droid.Render
{
    class MyProgressBarRenderer: ProgressBarRenderer
    {
        public MyProgressBarRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);

            Control.ProgressTintList = Android.Content.Res.ColorStateList.ValueOf(Color.Purple); //Change the color
            Control.ScaleY = 5; //Changes the height
        }
    }
}