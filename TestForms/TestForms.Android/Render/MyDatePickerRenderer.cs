﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using TestForms.Droid.Render;
using TestForms.Core.Custom;
using Android.Graphics.Drawables;
using Android.Graphics;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(MyDatePickerRenderer))]
namespace TestForms.Droid.Render
{
    class MyDatePickerRenderer:DatePickerRenderer
    {
        public MyDatePickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
               Control.Background.SetColorFilter(Color.Purple, PorterDuff.Mode.SrcAtop);
                
            }

            /*this.Control.SetTextColor(Android.Graphics.Color(#533f95));   
               this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            this.Control.SetPadding(20, 0, 0, 0);
            this.Control.SetBackgroundDrawable(gd);

            GradientDrawable gd = new GradientDrawable();
            SetCornerRadius(25); //increase or decrease to changes the corner look
            SetColor(Android.Graphics.Color.Transparent);
            SetStroke(3, Android.Graphics.Color.#533f95; */           
         

            CustomDatePicker element = Element as CustomDatePicker;
            if (!string.IsNullOrWhiteSpace(element.Placeholder))
            {
                Control.Text = element.Placeholder;
            }
            this.Control.TextChanged += (sender, arg) => {
                var selectedDate = arg.Text.ToString();
                if (selectedDate == element.Placeholder)
                {
                    Control.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }
            };
        }
    }
}