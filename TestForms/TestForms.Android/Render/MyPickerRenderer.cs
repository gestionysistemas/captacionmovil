﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using TestForms.Droid.Render;
using TestForms.Core.Custom;
using Android.Graphics.Drawables;
using Android.Graphics;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(MyPickerRenderer))]
namespace TestForms.Droid.Render
{
    class MyPickerRenderer:PickerRenderer
    {
        public MyPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            CustomPicker element = Element as CustomPicker;
            if (!string.IsNullOrWhiteSpace(element.Placeholder))
            {
                Control.Text = element.Placeholder;
            }

            if (e.NewElement != null)
            {
               Control.Background.SetColorFilter(Color.Purple, PorterDuff.Mode.SrcAtop);
                
            }
        }
    }
}