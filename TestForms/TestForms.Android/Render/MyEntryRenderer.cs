﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using TestForms.Droid.Render;

using Android.Graphics.Drawables;
using Android.Graphics;
using Color = Android.Graphics.Color;
using Android.Content.Res;
using TestForms.Core.Custom;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(MyEntryRenderer))]
namespace TestForms.Droid.Render
{
    class MyEntryRenderer:EntryRenderer
    {
        public MyEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null || e.NewElement == null) return;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.Purple);
            else
                Control.Background.SetColorFilter(Android.Graphics.Color.Purple, PorterDuff.Mode.SrcAtop);
            
        }
    }
}